package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

            String output = getExpression(statement);
            return counting(output);

    }

    static private boolean isDelimeter(char c){
        return " =".indexOf(c) != -1;
    }

    static private boolean isOperator(char c){
        return "+-/*()".indexOf(c) != -1;
    }

    static private byte getPriority(char operator){
        switch (operator)
        {
            case '(': return 0;
            case ')': return 1;
            case '+': return 2;
            case '-': return 3;
            case '*': return 4;
            case '/': return 5;
            default: return 6;
        }
    }

    static private String getExpression(String input) {

        String output = "";
        Stack<Character> operatorsStack = new Stack<>();

        try{
            if ("".equals(input)) throw new NullPointerException();

            for (int i = 0; i < input.length(); i++) {

                if (Character.isDigit(input.charAt(i))) {
                    while (!isOperator(input.charAt(i))) {
                        output += input.charAt(i);
                        i++;
                        if (i == input.length()) break;
                    }
                    output += " ";
                    i--;
                }

                if (isOperator(input.charAt(i))) {
                    if (input.charAt(i) == '(')
                        operatorsStack.push(input.charAt(i));
                    else if (input.charAt(i) == ')') {
                        Character s = operatorsStack.pop();
                        while (s != '(') {
                            output += s + " ";
                            s = operatorsStack.pop();
                        }
                    } else {
                        if (operatorsStack.size() > 0)
                            if (getPriority(input.charAt(i)) <= getPriority(operatorsStack.peek())) {
                                output += operatorsStack.pop().toString() + " ";
                            }
                        operatorsStack.push(input.charAt(i));
                    }
                }
            }
        } catch (NullPointerException | EmptyStackException e) {
            return null;
        }

        while (operatorsStack.size() > 0) {
            output += operatorsStack.pop() + " ";
        }
        return output;
    }

    static private String counting(String input)  {

        Stack<Double> tempStack = new Stack<>();

        try {
            if ("".equals(input)) throw new NullPointerException();

            for (int i = 0; i < input.length() ; i++) {

                if (Character.isDigit(input.charAt(i))){
                    String a = "";
                    while (!isDelimeter(input.charAt(i)) && !isOperator(input.charAt(i))){
                        a += input.charAt(i);
                        i++;
                        if (i == input.length()) break;
                    }
                    tempStack.push(Double.valueOf(a));
                    i--;
                } else if (isOperator(input.charAt(i))){
                    Double firstOperand = tempStack.pop();
                    Double secondOperand = tempStack.pop();
                    Double result = null;
                    switch (input.charAt(i)){
                        case '+':   result = secondOperand + firstOperand; break;
                        case '-':   result = secondOperand - firstOperand; break;
                        case '*':   result = secondOperand * firstOperand; break;
                        case '/':   result = secondOperand / firstOperand; break;
                    }

                    if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) {
                        throw new ArithmeticException();
                    }

                    tempStack.push(result);
                }
            }
        } catch (RuntimeException e) {
            return null;
        }

        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        DecimalFormat format = new DecimalFormat("###.####", decimalFormatSymbols);
        format.setRoundingMode(RoundingMode.HALF_UP);

        return format.format(tempStack.peek());
    }
}
