package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (cheksData(inputNumbers)){
            return createPyramid(sortListAsc(inputNumbers));
        } else throw new CannotBuildPyramidException();
    }

    private static boolean isPyramidPossible(List<Integer> inputNumbers){
        return (Math.sqrt(8*inputNumbers.size() + 1) - 1)/2 % 1 == 0 ;
    }

    private static boolean isInputDataContainsOnlyPrimitiveInt(List<Integer> inputNumbers){
        try{
            inputNumbers.stream().mapToInt(i -> i).toArray();
        } catch (NullPointerException e){
            return false;
        }
        return true;
    }

    private static boolean cheksData(List<Integer> inputNumbers){
        return  isPyramidPossible(inputNumbers) &&
                isInputDataContainsOnlyPrimitiveInt(inputNumbers);
    }

    private static List<Integer> sortListAsc(List<Integer> inputNumbers){
        Collections.sort(inputNumbers);
        return inputNumbers;
    }

    private static int[][] createPyramid(List<Integer> inputNumbers){

        int rows = (int) ((Math.sqrt(8*inputNumbers.size() + 1) - 1)/2);
        int columns = rows*2 - 1;
        int[][] pyramid = new int [rows][columns];

        int center = (columns/2);
        int howMuchArrayElementsInRow = 1;
        int inputNumberCounter = 0;

        for (int[] row : pyramid) {
            Arrays.fill(row, 0);
        }

        for (int i = 0, offset = 0; i < rows; i++, offset++, howMuchArrayElementsInRow++) {
            int start = center - offset;
            for (int j = 0; j < howMuchArrayElementsInRow*2; j +=2, inputNumberCounter++) {
                pyramid[i][start + j] = inputNumbers.get(inputNumberCounter);
            }
        }
        return pyramid;
    }
}
